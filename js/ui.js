function startTime() {
	var date = new Date();
	
	document.getElementById("bz-date-time").innerHTML = formatTime(date.getDate()) + "." +
														formatTime(date.getMonth() + 1) + "." + 
														date.getFullYear() + " | " + 
														formatTime(date.getHours()) + ":" + 
														formatTime(date.getMinutes()) + ":" + 
														formatTime(date.getSeconds()) + " Uhr";
														
	var timer = setTimeout(function() {
		startTime();
	}, 500);
}

function formatTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	
	return i;
}

function showLoadingScreen(show) {
	if (show) {
		$(".loading-bg").show();
	}
	else {
		$(".loading-bg").hide();
	}
}